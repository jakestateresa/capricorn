using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using Capricorn.Threading;
namespace Capricorn.Examples.ThreadedQueue
{
    public class CopyFileQueue : ThreadedQueueBase<CopyInfo>
    {
        Logger _logger;
        public CopyFileQueue(Logger logger)
        {
            _logger = logger;
        }

        protected override void ProcessItem(CopyInfo item)
        {
            //check if the user called Stop
            if (StopRequested)
            {
                _logger.Log(LogLevel.Information, "User called Stop.");
                _logger.Log(LogLevel.Information, 
                    "Terminating thread while copying directory '{0}'.",
                    item.Source);
                return;
            }

            _logger.Log(LogLevel.Debug, 
                "Checking if '{0}' exists...", item.Destination);                
            if (!Directory.Exists(item.Destination))
            {
                _logger.Log(LogLevel.Debug, 
                    "Creating '{0}'.", item.Destination);                
                Directory.CreateDirectory(item.Destination);
            }

            _logger.Log(LogLevel.Information, 
                "CopyFiles from '{0}' to '{1}' {2}...",
                item.Source,
                item.Destination,
                item.Recursive ? "recursive" : "non-recursive");

            foreach (string file in
                Directory.GetFiles(item.Source))
            {
                string destination = Path.Combine(
                    item.Destination,
                    Path.GetFileName(file));
                _logger.Log(LogLevel.Information, 
                    "Copying file from '{0}' to '{1}'...",
                    file,
                    destination);

                _logger.Log(LogLevel.Debug, 
                    "Copying file from '{0}' to '{1}'...", 
                    file, 
                    destination);                            
                File.Copy(file, destination, item.Overwrite);
            }

            if (item.Recursive)
            {
                foreach (string directory in
                    Directory.GetDirectories(item.Source))
                {
                    string destination = Path.Combine(
                        item.Destination,
                        Path.GetFileName(directory) //get the directory name for the path
                        );

                    _logger.Log(LogLevel.Debug, 
                        "Copying directory from '{0}' to '{1}'...", 
                        directory, 
                        destination);
                    EnqueueItem(
                        new CopyInfo(
                            directory,
                            destination,
                            item.Recursive,
                            item.Overwrite));

                }
            }            
        }
    }
}
