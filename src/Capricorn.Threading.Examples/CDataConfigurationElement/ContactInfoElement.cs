using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace Capricorn.Examples.CDataConfigurationElement
{
    public sealed class ContactInfoElement
        : ConfigurationElement
    {
        [ConfigurationProperty("name", IsRequired = true, IsKey = true)]
        public string Name
        {
            get { return (string)(base["name"]); }
            set { base["name"] = value; }
        }
        
        [ConfigurationProperty("email", IsRequired = true, IsKey = true)]
        public string Email
        {
            get { return (string)(base["email"]); }
            set { base["email"] = value; }
        }
    }
}
