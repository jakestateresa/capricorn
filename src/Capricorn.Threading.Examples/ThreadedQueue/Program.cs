using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

using Capricorn.Threading;
namespace Capricorn.Examples.ThreadedQueue
{
    class Program
    {
        private CopyInfo _copyInfo;
        public void Start()
        {
            _copyInfo = new CopyInfo(
                Environment.GetFolderPath(Environment.SpecialFolder.MyPictures),
                @"c:\pictures",
                true,
                true);
            
            Logger logger = new Logger(@"c:\",
                "SampleLog.log",
                "yyyy-MM-dd",
                LogLevel.LogAll,
                3,
                false,
                1024 /** 1024 */* 5);
            logger.Start();

            CopyFileQueue copyFileQueue = new CopyFileQueue(logger);
            copyFileQueue.Start();
            copyFileQueue.EnqueueItem(_copyInfo);

            Console.WriteLine("Press any key to exit...");
            
            Console.ReadKey(false);
            copyFileQueue.Stop(PendingItemAction.AbortPendingItems);
            logger.Stop(PendingItemAction.AbortPendingItems);            
        }
    }
}