using System;
using System.Configuration;
using System.Collections.Generic;
using System.Text;

using Capricorn.Configuration;
namespace Capricorn.Examples.CDataConfigurationElement
{
    [ConfigurationCollection(typeof(MailTemplateElement),
        CollectionType = ConfigurationElementCollectionType.BasicMap, 
        AddItemName="mailTemplate")]
    public sealed class MailTemplateElementCollection :
        ConfigurationElementCollection<MailTemplateElement>
    {
    } 
}

