using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace Capricorn.Examples.NewConfigurationSection
{
    class Program
    {
        public void Start()
        {
            System.Configuration.Configuration configuration = 
                ConfigurationManager.OpenExeConfiguration(
                    ConfigurationUserLevel.None);
            ProjectConfigurationSection configurationSection = null;
            ConfigurationSectionCollection sections = configuration.Sections;
            foreach (ConfigurationSection section in sections)
            {
                if (section is ProjectConfigurationSection)
                {
                    configurationSection = section 
                        as ProjectConfigurationSection;
                    break;
                }
            }

            if (configurationSection != null)
            {
                Console.WriteLine("Configured Users");
                foreach (UserElement user in configurationSection.Users)
                {
                    Console.WriteLine("User {0}: {1} {2}", 
                        user.Key, 
                        user.Firstname, 
                        user.Lastname);    
                }
                Console.ReadKey(false);
            }
        }
    }
}
