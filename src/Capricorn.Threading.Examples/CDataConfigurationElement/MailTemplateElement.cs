using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace Capricorn.Examples.CDataConfigurationElement
{
    public sealed class MailTemplateElement
        : ConfigurationElement
    {
        [ConfigurationProperty("name", IsRequired = true, IsKey = true)]
        public string Name
        {
            get { return (string)(base["name"]); }
            set { base["name"] = value; }
        }

        [ConfigurationProperty("sender", IsRequired = true)]
        public string Sender
        {
            get { return (string)(base["sender"]); }
            set { base["sender"] = value; }
        }

        [ConfigurationProperty("receivers",
            IsDefaultCollection = true, IsRequired = true)]
        public ContactInfoElementCollection Receivers
        {
            get { return (ContactInfoElementCollection)this["receivers"]; }
        }

        [ConfigurationProperty("body", IsRequired = true)]
        public MessageBodyElement Body
        {
            get { return (MessageBodyElement)(base["body"]); }
            set { base["body"] = value; }
        }
    }
}
