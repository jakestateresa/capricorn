using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;

namespace Capricorn.Examples.BasicThreading
{
    class Program
    {
        private CopyInfo _copyInfo;

        public void Start()
        {
            _copyInfo = new CopyInfo(
                Environment.GetFolderPath(Environment.SpecialFolder.MyPictures),
                @"c:\pictures",
                true,
                true);

            Thread simpleThread = new Thread(CopyFilesProc);
            simpleThread.Name = "CopyFiles";
            simpleThread.Start();
            simpleThread.Join();

            Thread parameterizedThread = new Thread(ParameterizedCopyFilesProc);
            parameterizedThread.Name = "CopyFiles";
            parameterizedThread.Start(_copyInfo);
            parameterizedThread.Join();
        }

        private void CopyFilesProc()
        {
            CopyFiles(_copyInfo);
        }

        private void ParameterizedCopyFilesProc(object state)
        {
            if (state is CopyInfo)
            {
                CopyFiles(state as CopyInfo);
            }
        }

        private void CopyFiles(CopyInfo copyInfo)
        {
            if (!Directory.Exists(copyInfo.Destination))
            {
                Directory.CreateDirectory(copyInfo.Destination);
            }

            Console.WriteLine("CopyFiles from '{0}' to '{1}' {2}...",
                copyInfo.Source,
                copyInfo.Destination,
                copyInfo.Recursive ? "recursive" : "non-recursive");

            foreach (string file in
                Directory.GetFiles(copyInfo.Source))
            {
                string destination = Path.Combine(
                    copyInfo.Destination,
                    Path.GetFileName(file));
                File.Copy(file, destination, copyInfo.Overwrite);
            }

            if (copyInfo.Recursive)
            {
                foreach (string directory in
                    Directory.GetDirectories(copyInfo.Source))
                {
                    string destination = Path.Combine(
                        copyInfo.Destination,
                        Path.GetFileName(directory) //get the directory name for the path
                        );

                    CopyFiles(
                        new CopyInfo(
                            directory,
                            destination,
                            copyInfo.Recursive,
                            copyInfo.Overwrite));

                }
            }

            Console.WriteLine("CopyFiles finished.");
        }
    }
}
