using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using Capricorn.Threading;
namespace Capricorn.Examples.WorkerThread
{
    public class CopyFileWorker : WorkerThreadBase
    {
        private static CopyInfo _copyInfo;

        public CopyFileWorker(CopyInfo copyInfo)
        {
            _copyInfo = copyInfo;
        }

        protected override void Work()
        {
            copyFiles(_copyInfo);
        }

        private void copyFiles(CopyInfo copyInfo)
        {
            //check if the user called Stop
            if (StopRequested)
            {
                Console.WriteLine("User called Stop.");
                Console.WriteLine(
                    "Terminating thread while copying directory '{0}'.", 
                    copyInfo.Source);
                return;
            }

            if (!Directory.Exists(copyInfo.Destination))
            {
                Directory.CreateDirectory(copyInfo.Destination);
            }

            Console.WriteLine("CopyFiles from '{0}' to '{1}' {2}...",
                copyInfo.Source,
                copyInfo.Destination,
                copyInfo.Recursive ? "recursive" : "non-recursive");

            foreach (string file in
                Directory.GetFiles(copyInfo.Source))
            {
                string destination = Path.Combine(
                    copyInfo.Destination,
                    Path.GetFileName(file));
                File.Copy(file, destination, copyInfo.Overwrite);
            }

            if (copyInfo.Recursive)
            {
                foreach (string directory in
                    Directory.GetDirectories(copyInfo.Source))
                {
                    string destination = Path.Combine(
                        copyInfo.Destination,
                        Path.GetFileName(directory) //get the directory name for the path
                        );

                    copyFiles(
                        new CopyInfo(
                            directory,
                            destination,
                            copyInfo.Recursive,
                            copyInfo.Overwrite));

                }
            }

            Console.WriteLine("CopyFiles finished.");
        }

    }
}
