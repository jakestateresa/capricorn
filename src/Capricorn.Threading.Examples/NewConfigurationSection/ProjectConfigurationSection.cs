using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;

namespace Capricorn.Examples.NewConfigurationSection
{
    public sealed class ProjectConfigurationSection : 
        ConfigurationSection
    {
        [ConfigurationProperty("users", 
            IsDefaultCollection = true, IsRequired = true)]
        internal UserElementCollection Users
        {
            get { return (UserElementCollection)this["users"]; }
        }
    }
}
