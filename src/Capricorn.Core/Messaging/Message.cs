namespace Capricorn.Messaging
{
    public class Message
    {
        private MessageHeader _messageHeader;
        private object _body;

        internal Message(){}

        public Message(object body)
            : this(new MessageHeader(), body)
        {
        }

        public Message(
            MessageHeader messageHeader,
            object body)
        {
            _messageHeader = messageHeader;
            _body = body;
        }

        public MessageHeader Header
        {
            get { return _messageHeader; }
            internal set { _messageHeader = value; }
        }

        public object Body
        {
            get { return _body; }
            internal set { _body = value; }
        }
    }

    public class Message<T>:
        Message
    {
        public Message(T body)
        {
            Header = new MessageHeader();
            base.Body = body;
        }        

        public Message(
            MessageHeader messageHeader,
            T body)
        {
            Header = messageHeader;
            base.Body = body;
        }

        public new T Body
        {
            get { return (T)base.Body; }            
        }
    }
}
