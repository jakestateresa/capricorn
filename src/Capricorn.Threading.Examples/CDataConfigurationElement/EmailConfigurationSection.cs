using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace Capricorn.Examples.CDataConfigurationElement
{
    public sealed class EmailConfigurationSection
        : ConfigurationSection
    {
        [ConfigurationProperty("server", IsRequired = true, IsKey = true)]
        public string Server
        {
            get { return (string)(base["server"]); }
            set { base["server"] = value; }
        }

        [ConfigurationProperty("port", IsRequired = true)]
        public int Port
        {
            get { return (int)(base["port"]); }
            set { base["port"] = value; }
        }

        [ConfigurationProperty("mailTemplates",
            IsDefaultCollection = true, IsRequired = true)]
        public MailTemplateElementCollection MailTemplates
        {
            get { return (MailTemplateElementCollection)this["mailTemplates"]; }
        }
    }
}
