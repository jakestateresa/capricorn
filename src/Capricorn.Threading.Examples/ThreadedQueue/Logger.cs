using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using Capricorn.Threading;
namespace Capricorn.Examples.ThreadedQueue
{
    public class Logger : ThreadedQueueBase<LogEntry>
    {
        private LogLevel _logLevel;
        private string  _logPath;
        private string _filename;
        private int _maximumFileAgeInDays;
        private string _dateFormat;
        private StreamWriter _streamWriter;
        private bool _keepFileOpen;
        private long _rollingFileSize;
        private DateTime _lastCleanUpDate;
                
        public Logger(string logPath,
            string filename) 
            : this(logPath, filename, "dd-MMM-yyyy",
            LogLevel.Warning | LogLevel.Error,
            3,            
            false,
            1024 * 1024)
        { }

        public Logger(
            string logPath,
            string filename,
            string dateFormat,
            LogLevel logLevel,
            int maximumFileAgeInDays,
            bool keepFileOpen,
            long rollingFileSize)
        {
            _logLevel = logLevel;
            _logPath = logPath;
            _filename = filename;
            _maximumFileAgeInDays = maximumFileAgeInDays;
            _dateFormat = dateFormat;
            _keepFileOpen = keepFileOpen;
            _rollingFileSize = rollingFileSize;
            _lastCleanUpDate = new DateTime();            
            
            performCleanUp();
        }
        
        public string DateFormat
        {
            get { return _dateFormat; }
        }        

        public LogLevel LogLevel
        {
            get { return _logLevel; }            
        }

        public string LogPath
        {
            get { return _logPath; }
        }

        public string Filename
        {
            get { return _filename; }
        }

        public int MaximumFileAgeInDays
        {
            get { return _maximumFileAgeInDays; }
        }        

        public long RollingFileSize
        {
            get { return _rollingFileSize; }            
        }

        public void Log(LogLevel loglevel,
            string message,
            params object[] parameters)
        {
            LogEntry logEntry = new LogEntry(
                        loglevel,
                        message,
                        parameters);
            EnqueueItem(logEntry);
        }

        protected override void ProcessItem(LogEntry item)
        {            
            bool loggable = 
                (_logLevel & item.LogLevel) == item.LogLevel;
            if (!loggable)
            {
                return;
            }

            string formattedDate = 
                DateTime.Now.ToString(_dateFormat);
            string formattedFilename = string.Format("{0}.{1}", 
                formattedDate, 
                _filename);
            string actualFilename = Path.Combine(
                LogPath, 
                formattedFilename);

            if (_streamWriter == null)
            {
                initializeStreamWriter(actualFilename);
            }

            // exceeds the rolling file size?
            // create a back-up file in the form
            // dd-MMM-yyyy.filename.x where x is 
            // the total number of files which starts with that name
            if (_streamWriter.BaseStream.Length >= _rollingFileSize)
            {
                _streamWriter.Close();
                _streamWriter = null;

                string[] files = Directory.GetFiles(LogPath, formattedFilename + "*");
                string backupFilename = actualFilename + "." + files.Length;

                if (File.Exists(backupFilename))
                {
                    File.Delete(backupFilename);
                }
                File.Move(actualFilename, backupFilename);
                initializeStreamWriter(actualFilename);                
            }

            _streamWriter.WriteLine("[{0}] {1} {2} {3} - {4}", 
                System.Diagnostics.Process.GetCurrentProcess().ProcessName,
                item.DateCreated.ToString(),
                item.LogLevel.ToString(),
                System.Threading.Thread.CurrentPrincipal.Identity.Name,
                item.Message);
            _streamWriter.Flush();

            performCleanUp();

            if (!_keepFileOpen)
            {
                _streamWriter.Close();
                _streamWriter = null;
            }
        }

        private void initializeStreamWriter(string filename)
        {
            _streamWriter = new StreamWriter(
                new FileStream(filename,
                    FileMode.OpenOrCreate,
                    FileAccess.ReadWrite,
                    FileShare.Read, 1024,
                    FileOptions.WriteThrough));
            _streamWriter.BaseStream.Seek(
                _streamWriter.BaseStream.Length,
                SeekOrigin.Begin);
        }

        private void performCleanUp()
        {
            if (DateTime.Now.Date == _lastCleanUpDate)
            {
                return;
            }

            string logPath = LogPath;
            string[] files = Directory.GetFiles(
                logPath, 
                "*" + _filename + "*");
            foreach (string file in files)
            {
                FileInfo fileInfo = new FileInfo(file);
                TimeSpan fileAge = DateTime.Now.Date.Subtract(fileInfo.CreationTime.Date);
                if (fileAge.Days >= _maximumFileAgeInDays)
                {
                    File.Delete(file);
                }
            }
            _lastCleanUpDate = DateTime.Now.Date;
        }
    }
}
