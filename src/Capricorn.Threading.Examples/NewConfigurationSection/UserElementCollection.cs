using System;
using System.Configuration;
using System.Collections.Generic;
using System.Text;

using Capricorn.Configuration;
namespace Capricorn.Examples.NewConfigurationSection
{
    [ConfigurationCollection(typeof(UserElement),
        CollectionType = ConfigurationElementCollectionType.BasicMap)]
    internal sealed class UserElementCollection : 
        ConfigurationElementCollection<UserElement>
    {
    } 
}
