using System;
using System.Threading;
using Capricorn.Threading;

namespace Capricorn.Messaging
{
    public class Dispatcher : ThreadedQueueBase<Message>
    {
        private readonly int _threadsPerProcessor;

        private const int DEFAULT_THREADS_PER_PROCESSOR = 2;

        public Dispatcher()
            : this(null, ThreadPriority.Normal, DEFAULT_THREADS_PER_PROCESSOR)
        {
        }

        public Dispatcher(int threadPerProcessor)
            : this(null, ThreadPriority.Normal, threadPerProcessor)
        {
        }

        public Dispatcher(string name,
            int threadPerProcessor)
            : this(name, ThreadPriority.Normal, threadPerProcessor)
        {
        }

        public Dispatcher(string name,
            ThreadPriority priority,
            int threadPerProcessor)
            : this(name, priority, false, threadPerProcessor)
        {
        }

        public Dispatcher(string name,
            ThreadPriority priority,
            bool isBackground,
            int threadPerProcessor)
            : base(name, priority, isBackground)
        {
            _threadsPerProcessor = threadPerProcessor;
        }

        public int ThreadsPerProcessor
        {
            get { return _threadsPerProcessor; }
        }

        protected override void ProcessItem(Message item)
        {            
        }
    }
}
