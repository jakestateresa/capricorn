using System;
using System.Collections.Generic;
using System.Text;

using Capricorn.Threading;
namespace Capricorn.Examples.WorkerThread
{
    class Program
    {
        private CopyInfo _copyInfo;        
        public void Start()
        {
            _copyInfo = new CopyInfo(
                Environment.GetFolderPath(Environment.SpecialFolder.MyPictures), 
                @"c:\pictures", 
                true,
                true);

            DummyWorker dummyWorker = new DummyWorker();
            dummyWorker.Start();

            CopyFileWorker copyFileWorker = new CopyFileWorker(_copyInfo);
            copyFileWorker.Start();

            //wait for the two threads to finish
            WorkerThreadBase.WaitAll(copyFileWorker, dummyWorker);
        }
    }
}
