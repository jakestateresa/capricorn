using System;
using System.Collections.Generic;
using System.Text;

namespace Capricorn.Examples.ThreadedQueue
{
    [Flags]
    public enum LogLevel
    {
        Information = 1,
        Debug = 2,
        Warning = 4,
        Error = 8,
        LogAll = Information | Debug | Warning | Error
    }
}
