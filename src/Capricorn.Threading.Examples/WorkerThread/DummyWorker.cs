using System;
using System.Collections.Generic;
using System.Text;

using Capricorn.Threading;
namespace Capricorn.Examples.WorkerThread
{
    public class DummyWorker:WorkerThreadBase
    {
        protected override void Work()
        {
            //a relatively long task
            System.Threading.Thread.Sleep(10000);

            if (StopRequested)
            {
                return;
            }

            //a relatively long task
            System.Threading.Thread.Sleep(5000);

            Console.WriteLine("Dummy worker completed.");
        }
    }
}
