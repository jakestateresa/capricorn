using System;
using System.Configuration;
using System.Collections.Generic;
using System.Text;

using Capricorn.Configuration;
namespace Capricorn.Examples.CDataConfigurationElement
{
    [ConfigurationCollection(typeof(ContactInfoElement),
        CollectionType = ConfigurationElementCollectionType.BasicMap, 
        AddItemName="contactInfo")]
    public sealed class ContactInfoElementCollection :
        ConfigurationElementCollection<ContactInfoElement>
    {
    } 
}

