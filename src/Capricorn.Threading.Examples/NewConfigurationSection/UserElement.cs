using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;

namespace Capricorn.Examples.NewConfigurationSection
{
    internal class UserElement : ConfigurationElement
    {
        public UserElement()
        {            
        }

        public UserElement(string key)
        {
            Key = key;
        }

        [ConfigurationProperty("key", IsRequired = true, IsKey = true)]
        public string Key
        {
            get { return (string)(base["key"]); }
            set { base["key"] = value; }
        }

        [ConfigurationProperty("firstname", IsRequired = true)]
        public string Firstname
        {
            get { return (string)(base["firstname"]); }
            set { base["firstname"] = value; }
        }

        [ConfigurationProperty("lastname", IsRequired = true)]
        public string Lastname
        {
            get { return (string)(base["lastname"]); }
            set { base["lastname"] = value; }
        }  
    }
}
