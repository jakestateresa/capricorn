using System;
using System.Collections.Generic;
using System.Text;

namespace Capricorn.Examples.BasicThreading
{
    public class CopyInfo
    {
        private string _source;
        private bool _recursive;
        private string _destination;
        private bool _overwrite;        

        public CopyInfo(
            string source,
            string destination,
            bool recursive,
            bool overwrite)
        {
            _source = source;
            _destination = destination;
            _recursive = recursive;
            _overwrite = overwrite;
        }

        public string Source
        {
            get { return _source; }            
        }
        
        public string Destination
        {
            get { return _destination; }            
        }

        public bool Recursive
        {
            get { return _recursive; }            
        }

        public bool Overwrite
        {
            get { return _overwrite; }            
        }
    }
}
