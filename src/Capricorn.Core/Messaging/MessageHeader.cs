using System;
using System.Threading;

namespace Capricorn.Messaging
{
    public class MessageHeader
    {
        private long _messageId;
        private DateTime _timestamp;
        private string _username;

        public MessageHeader()

        {
            _messageId = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 0);
            _timestamp = DateTime.UtcNow;
            _username = Thread.CurrentPrincipal.Identity.Name;
        }

        public MessageHeader(
            long messageId,
            DateTime timestamp,
            string username
            )
        {
            _messageId = messageId;
            _timestamp = timestamp;
            _username = username;
        }

        public long MessageId
        {
            get { return _messageId; }
        }

        public DateTime Timestamp
        {
            get { return _timestamp; }
        }

        public string Username
        {
            get { return _username; }            
        }
    }
}
