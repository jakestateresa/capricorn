using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

using Capricorn.Configuration;
using cdataElement = Capricorn.Configuration.CDataConfigurationElement;
namespace Capricorn.Examples.CDataConfigurationElement
{
    public class MessageBodyElement
        : cdataElement
    {
        [ConfigurationProperty("content", IsRequired = true, IsKey=true)]
        [CDataConfigurationProperty]
        public string Content
        {
            get { return (string)(base["content"]); }
            set { base["content"] = value; }
        }        
    }
}
