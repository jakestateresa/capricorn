using System;
using System.Collections.Generic;
using System.Text;

namespace Capricorn.Examples.ThreadedQueue
{
    public class LogEntry
    {
        private DateTime _dateCreated;
        private string _message;
        private LogLevel _logLevel;

         public LogEntry(LogLevel logLevel,        
            string message,
            params object[] args)
            : this(logLevel, message)
        {
            _message = string.Format(message, args);
        }

        public LogEntry(LogLevel logLevel,            
            string message)
        {
            _dateCreated = DateTime.Now;
            _message = message;
            _logLevel = logLevel;            
        }

        public DateTime DateCreated
        {
            get { return _dateCreated; }        
        }

        public string Message
        {
            get { return _message; }        
        }

        public LogLevel LogLevel
        {
            get { return _logLevel; }        
        }
    }
}
