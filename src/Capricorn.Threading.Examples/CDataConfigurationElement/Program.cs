using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace Capricorn.Examples.CDataConfigurationElement
{
    class Program
    {
        public void Start()
        {
            System.Configuration.Configuration configuration = 
                ConfigurationManager.OpenExeConfiguration(
                    ConfigurationUserLevel.None);
            EmailConfigurationSection configurationSection = null;
            ConfigurationSectionCollection sections = configuration.Sections;
            foreach (ConfigurationSection section in sections)
            {
                if (section is EmailConfigurationSection)
                {
                    configurationSection = section
                        as EmailConfigurationSection;
                    break;
                }
            }

            if (configurationSection != null)
            {
                Console.WriteLine("Server Information: ");
                Console.WriteLine("Host: {0}", configurationSection.Server);
                Console.WriteLine("Port: {0}", configurationSection.Port);
                Console.WriteLine("MailTemplate");
                foreach (MailTemplateElement mailTemplate in
                   configurationSection.MailTemplates)
                {
                    Console.WriteLine("Name: {0}", mailTemplate.Name);
                    Console.WriteLine("Sender: {0}", mailTemplate.Sender);
                    foreach (ContactInfoElement contactInfo in
                        mailTemplate.Receivers)
                    {
                        Console.WriteLine("Receiver Name: {0} Email: {1}", 
                            contactInfo.Name,
                            contactInfo.Email);
                    }
                    Console.WriteLine("Body: {0}", mailTemplate.Body.Content);
                    Console.WriteLine("=====================================");
                }                                

                Console.ReadKey(false);
            }
        }
    }
}

